import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';

abstract class Utils {
  static void showAdminDialog(
      {required BuildContext context,
      required Widget child,
      required String title}) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          scrollable: true,
          title: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      Navigator.pop(context); // Cierra el diálogo
                    },
                  )
                ],
              ),
              TextComponent(
                title,
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall
                    ?.copyWith(fontWeight: FontWeight.bold),
              )
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min, // Ajusta el alto del contenido
            children: [
              child,
            ],
          ),
          contentPadding:
              const EdgeInsets.all(16.0), // Ajusta el espacio del contenido
          titlePadding:
              const EdgeInsets.all(16.0), // Ajusta el espacio del título
          actionsPadding: const EdgeInsets.only(top: 8.0, right: 8.0),
          /*actions: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context); // Cierra el diálogo
              },
            ),
          ),
        ],*/
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(16.0), // Ajusta la forma del diálogo
          ),
        );
      },
    );
  }

  static Widget adjustableTextComponent(
      {required BuildContext context,
        required List<String> texts,
      bool firtsTextBold = false,
      double width = 175.0}) {
    return SizedBox(
      width: width,
      child: RichText(
        //textAlign: TextAlign.left,
        text: TextSpan(
          style: const TextStyle(color: Colors.black),
          children: [
            TextSpan(
              children: [
                for (int i = 0; i < texts.length; i++)
                  TextSpan(
                    text: texts[i],
                    style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                        fontWeight:
                            (i == 0 && firtsTextBold) ? FontWeight.bold : null),
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  static Widget currencyFormat(double mount) {
    return TextComponent(_currencyFormatString(mount));
  }

  static String currencyFormatToString(double mount) {
    return _currencyFormatString(mount);
  }
}

String _currencyFormatString(double mount) {
    String resultado = '';
    int contador = 0;
    int idexOfvalue = mount.toString().indexOf('.');

    List<String> caracteresList = mount.toString().substring(0, idexOfvalue == -1 ? mount.toString().length : idexOfvalue).split('').reversed.toList();

    for (int i = 0; i < caracteresList.length; i++) {
      resultado = caracteresList[i] + resultado;
      contador++;
      if (contador == 3 && i != caracteresList.length - 1) {
        resultado = ',$resultado';
        contador = 0;
      }
    }

    if (mount.toString().contains('.')) {
      String decimals = mount.toString().substring(idexOfvalue == -1 ? mount.toString().length : idexOfvalue, mount.toString().length);

      double decimalResult = double.parse(decimals);

      decimalResult = double.parse(decimalResult.toStringAsFixed(2));

      decimals = decimalResult.toString();

      decimals = decimals.substring(decimals.indexOf('.'), decimals.length);

      if (decimals.length == 2) {
        decimals = '${decimals}0';
      }


      resultado = '$resultado$decimals';
    }
    else {
      resultado = '$resultado.00';
    }

    return resultado;
  }