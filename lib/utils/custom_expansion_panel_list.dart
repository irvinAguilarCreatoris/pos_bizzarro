import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pos_bizzarro/utils/utils.dart';

class CustomExpansionPanelList extends StatefulWidget {
  final List<CustomExpansionPanel> children;

  const CustomExpansionPanelList({
    super.key,
    required this.children,
  });

  @override
  State<CustomExpansionPanelList> createState() =>
      _CustomExpansionPanelListState();
}

class _CustomExpansionPanelListState extends State<CustomExpansionPanelList> {
  late bool isExpanded; // Quita el modificador final
  late String headerValue; // Quita el modificador final
  late StreamController<_SingletonI> streamController;
  late StreamSubscription<_SingletonI> streamSubscription;

  @override
  void initState() {
    super.initState();

    List<_SingletonI> list = [];

    streamController = StreamController<_SingletonI>();

    _Singleton().setStreamController = streamController;

    streamSubscription = streamController.stream.listen((_SingletonI stream) {
      Iterable<_SingletonI> listFiltered =
          list.where((element) => element.index == stream.index).toList();

      if (listFiltered.isEmpty) {
        list.add(stream);
      }

      _Singleton().setFn(list);

      for (var widgt in _Singleton().getFn()) {
        widgt.fn.call(reset: true);
      }

      stream.fn.call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: widget.children,
    );
  }

  @override
  Future<void> dispose() async {
    streamSubscription.cancel();
    _Singleton().resetIndex();
    super.dispose();
  }
}

// ignore: must_be_immutable
class CustomExpansionPanel extends StatefulWidget {
  bool isExpanded;
  final String headerValue;
  final Widget child;

  CustomExpansionPanel(
      {super.key,
      this.isExpanded = false,
      required this.child,
      required this.headerValue});

  @override
  State<CustomExpansionPanel> createState() => _CustomExpansionPanelState();
}

class _CustomExpansionPanelState extends State<CustomExpansionPanel> {
  late bool isExpanded; // Quita el modificador final
  late String headerValue; // Quita el modificador final
  int index = 0;

  @override
  void initState() {
    super.initState();
    isExpanded = widget.isExpanded;
    headerValue = widget.headerValue;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Utils.adjustableTextComponent(
                  context: context, texts: [headerValue]),
              InkWell(
                onTap: () {
                  if (index == 0) {
                    index = _Singleton().getIndex();
                  }

                  fn({bool reset = false}) => setState(() {
                        if (reset) {
                          isExpanded = false;
                        } else {
                          isExpanded = !isExpanded;
                        }
                      });
                  _Singleton().setDataStreamController(_SingletonI(fn, index));
                  setState(() {});
                },
                child: Icon(
                  isExpanded
                      ? Icons.radio_button_checked
                      : Icons.radio_button_off,
                ),
              ),
            ],
          ),
        ),
        AnimatedCrossFade(
          firstChild: Container(),
          secondChild: widget.child,
          crossFadeState:
              isExpanded ? CrossFadeState.showSecond : CrossFadeState.showFirst,
          duration: const Duration(milliseconds: 300),
        ),
      ],
    );
  }
}

class _SingletonI {
  late void Function({bool reset}) fn;
  late int index;

  _SingletonI(this.fn, this.index);
}

class _Singleton {
  List<_SingletonI> fn;
  int indexPosition = 0;
  late StreamController<_SingletonI> streamController;
  // Instancia única de la clase
  static final _Singleton _instance = _Singleton._internal([]);

  // Constructor privado
  _Singleton._internal(this.fn);

  // Método para obtener la instancia única
  factory _Singleton() {
    return _instance;
  }

  set setStreamController(StreamController<_SingletonI> value) {
    streamController = value;
  }

  int getIndex() {
    indexPosition++;
    return indexPosition;
  }

  void resetIndex() {
    indexPosition = 0;
  }

  void setFn(List<_SingletonI> funcion) {
    fn = funcion;
  }

  List<_SingletonI> getFn() {
    return fn;
  }

  void setDataStreamController(_SingletonI stream) {
    streamController.sink.add(stream);
  }
}
