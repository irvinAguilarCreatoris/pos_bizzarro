import 'package:flutter/material.dart';

import 'package:pos_bizzarro/models/models.dart';
import 'package:pos_bizzarro/pages/pages.dart';

class AppRoutes {

  static const initialRoute = 'login';

  static final menuOptions = <MenuOptionModel>[
    MenuOptionModel(route: 'venta', name: 'Venta', page: const VentaPage(), icon: Icons.list_alt ),
    MenuOptionModel(route: 'articulos', name: 'Articulos', page: const ArticulosPage(), icon: Icons.list ),
    MenuOptionModel(route: 'cliente', name: 'Cliente', page: const ClienteDetallePage(), icon: Icons.credit_card ),
    MenuOptionModel(route: 'carrito', name: 'Carrito', page: const CarritoPage(), icon: Icons.supervised_user_circle_outlined ),
    MenuOptionModel(route: 'pago', name: 'Pago', page: const PagoPage(), icon: Icons.supervised_user_circle_outlined ),
  ];


  static Map<String, Widget Function(BuildContext)> getAppRoutes() {

    Map<String, Widget Function(BuildContext)> appRoutes = {};
    appRoutes.addAll({ 'login' : ( BuildContext context ) => const LoginPage() });
    appRoutes.addAll({ 'home' : ( BuildContext context ) => const HomePage() });

    for (final option in menuOptions ) {
      appRoutes.addAll({ option.route : ( BuildContext context ) => option.page });
    }

    return appRoutes;
  }

  static Route<dynamic> onGenerateRoute( RouteSettings settings) {        
      return MaterialPageRoute(
          builder: (context) => const HomePage(),
      );
  }

}