import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';

class SelectInput extends StatefulWidget {
  final List<String> items;
  final String selectedValue;

  const SelectInput(
      {super.key, required this.items, required this.selectedValue});

  @override
  State<SelectInput> createState() => _SelectInputState();
}

class _SelectInputState extends State<SelectInput> {
  late List<String> items;
  late String selectedValue;

  @override
  void initState() {
    super.initState();
    items = widget.items; // Asigna items aquí en el initState
    selectedValue = widget.selectedValue;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        minWidth: 550,
        maxWidth: 750,
      ),
      height: 55,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
          color: Colors.grey,
          width: 1.0,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 9.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            value: selectedValue,
            icon: const Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            style: const TextStyle(color: Colors.black),
            onChanged: (String? newValue) {
              setState(() {
                selectedValue = newValue!;
              });
            },
            items: items.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: TextComponent(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
