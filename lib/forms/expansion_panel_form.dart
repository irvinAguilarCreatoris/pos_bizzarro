import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/models/models.dart';

class ExpansionPanelForm extends StatefulWidget {
  final List<ItemExpansionPanelModel> itemExpansionPanel;
  final bool formRadio;
  const ExpansionPanelForm(
      {super.key, required this.itemExpansionPanel, required this.formRadio});

  @override
  State<ExpansionPanelForm> createState() => _ExpansionPanelFormState();
}

class _ExpansionPanelFormState extends State<ExpansionPanelForm> {
  late List<ItemExpansionPanelModel> _data;

  @override
  void initState() {
    super.initState();
    _data = widget.itemExpansionPanel; // Asigna items aquí en el initState
  }

  @override
  Widget build(BuildContext context) {
    Widget widget;
    widget = SingleChildScrollView(
      child: ExpansionPanelList(
        elevation: 1,
        expandedHeaderPadding: const EdgeInsets.all(0),
        expansionCallback: (int index, bool isExpanded) {
          setState(() {
            for (var i in _data) {
              i.isExpanded = false;
            }

            _data[index].isExpanded = isExpanded;
          });
        },
        children: _data.map<ExpansionPanel>((ItemExpansionPanelModel item) {
          return ExpansionPanel(
            headerBuilder: (BuildContext context, bool isExpanded) {
              return ListTile(
                title: TextComponent(item.headerValue),
              );
            },
            body: item.expandedValue,
            isExpanded: item.isExpanded,
          );
        }).toList(),
      ),
    );

    return widget;
  }
}
