import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';

class Buttons {
  static Widget back({required void Function()? onPressed}) {
    return Buttons.buttonShape(
        prefixIcon: Icons.keyboard_backspace_outlined,
        verticalPadding: 8,
        labelText: 'Volver',
        underlineLabelText: true,
        onPressed: onPressed,
        type: 'accent');
  }

  static Widget addClient(
      {required Size size,
      required void Function()? onPressed,
      String type = 'basic'}) {
    return Buttons.buttonShape(
        prefixIcon: Icons.person_outline,
        verticalPadding: 8,
        labelText:
            size.width >= 1220 ? 'Añadir nuevo cliente' : 'Nuevo cliente',
        onPressed: onPressed,
        type: type);
  }

  static Widget buttonShape(
      {required String labelText,
      String type = 'basic',
      double horizontalPadding = 0,
      IconData? suffixIcon,
      bool underlineLabelText = false,
      IconData? prefixIcon,
      double verticalPadding = 0,
      required void Function()? onPressed,
      double? minWidth}) {
    Color? buttonColor;
    Color textColor;

    switch (type) {
      case 'primary':
        buttonColor = Colors.deepPurple;
        textColor = Colors.white;
        break;
      case 'accent':
        buttonColor = null;
        textColor = Colors.deepPurple;
        break;
      case 'warning':
        buttonColor = Colors.red;
        textColor = Colors.white;
        break;
      case 'basic':
        buttonColor = Colors.white;
        textColor = Colors.deepPurple;
        break;
      default:
        buttonColor = Colors.white;
        textColor = Colors.deepPurple;
    }
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      disabledColor: Colors.grey,
      elevation: 0,
      color: buttonColor,
      onPressed: onPressed,
      minWidth: minWidth,
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: horizontalPadding, vertical: verticalPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            prefixIcon != null
                ? Icon(
                    prefixIcon,
                    color: textColor,
                  )
                : const SizedBox(
                    width: 10,
                  ),
            TextComponent(
              labelText,
              style: TextStyle(
                  color: textColor,
                  decoration:
                      underlineLabelText ? TextDecoration.underline : null),
            ),
            suffixIcon != null
                ? Icon(
                    suffixIcon,
                    color: textColor,
                  )
                : const SizedBox(
                    width: 10,
                  ),
          ],
        ),
      ),
    );
  }
}
