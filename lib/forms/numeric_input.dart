import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/models/models.dart';
import 'package:pos_bizzarro/providers/articulos_provider.dart';
import 'package:provider/provider.dart';

class NumericInput extends StatefulWidget {
  final String itemKey;
  const NumericInput({super.key, required this.itemKey});

  @override
  State<NumericInput> createState() => _NumericInputState();
}

class _NumericInputState extends State<NumericInput> {
  late int _value;

  void _increment(ArticulosProvider articulosProvider) {
    setState(() {
      ItemSelectedModel? model = articulosProvider.getItemsSelected[widget.itemKey];

      articulosProvider.setChangeCountValue = _value + 1;
      if (model != null) {
        articulosProvider.setItemSelected(
            widget.itemKey,
            ItemSelectedModel(
                id: model.id,
                title: model.title,
                model: model.model,
                price: model.price,
                count: articulosProvider.getChangeCountValue));
      }
    });
  }

  void _decrement(ArticulosProvider articulosProvider) {
    setState(() {
      ItemSelectedModel? model = articulosProvider.getItemsSelected[widget.itemKey];

      if (_value > 1) {
        articulosProvider.setChangeCountValue = _value - 1;

        if (model != null) {
        articulosProvider.setItemSelected(
            widget.itemKey,
            ItemSelectedModel(
                id: model.id,
                title: model.title,
                model: model.model,
                price: model.price,
                count: articulosProvider.getChangeCountValue));
      }
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final articulosProvider = Provider.of<ArticulosProvider>(context);
    _value = articulosProvider.getItemsSelected[widget.itemKey] != null ? articulosProvider.getItemsSelected[widget.itemKey]!.count : 1;
    return Row(
      children: [
        IconButton(
          onPressed: () => _decrement(articulosProvider),
          icon: const Icon(Icons.remove),
        ),
        TextComponent('$_value'),
        IconButton(
          onPressed: () => _increment(articulosProvider),
          icon: const Icon(Icons.add),
        ),
      ],
    );
  }
}
