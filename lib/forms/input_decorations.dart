import 'package:flutter/material.dart';

class InputDecorations {
  static InputDecoration inputDecoration({
    String hintText = '',
    String labelText = '',
    bool isPassword = false,
    bool isPasswordVisible = false,
    VoidCallback? togglePassword,
    Widget? suffixIcon,
    bool filled = false
  }) {
    return InputDecoration(
      filled: filled,
      fillColor: Colors.white,
      labelText: labelText,
      hintText: hintText,
      suffixIcon: isPassword
          ? isPasswordVisible
              ? IconButton(
                  icon: const Icon(Icons.visibility),
                  onPressed: togglePassword,
                )
              : IconButton(
                  icon: const Icon(Icons.visibility_off),
                  onPressed: togglePassword,
                )
          : suffixIcon,
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: const BorderSide(
          color: Colors.deepPurple,
          width: 1.0,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5.0),
        borderSide: const BorderSide(
          color: Colors.grey,
          width: 1.0,
        ),
      ),
    );
  }
}
