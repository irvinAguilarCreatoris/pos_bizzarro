import 'package:flutter/material.dart';
import 'package:pos_bizzarro/models/forma_pago_selected_model.dart';

class PagosProvider extends ChangeNotifier {
  String _metodoPagoValue = '';
  String _bankIntefaz = '';
  final Map<String, FormaPagoSelectedModel> _formaPagoSelected = {};
  double _mountTopay = 0;

  String get getMetodoPagoValue {
    return _metodoPagoValue;
  }

  set setMetodoPagoValue(String value) {
    _metodoPagoValue = value;
    notifyListeners();
  }

  double get getMountToPay {
    return _mountTopay;
  }

  set setMountToPay(double value) {
    _mountTopay = value;
    notifyListeners();
  }

  String get getBankIntefaz {
    return _bankIntefaz;
  }

  set setBankIntefaz(String value) {
    _bankIntefaz = value;
    notifyListeners();
  }

  void setItemSelected(String key, FormaPagoSelectedModel model) {
    _formaPagoSelected[key] = model;
    notifyListeners();
  }

  void deleteItem(String key) {
    if (_formaPagoSelected.containsKey(key)) {
      _formaPagoSelected.remove(key);
    }
    notifyListeners();
  }

  Map<String, FormaPagoSelectedModel> get getFormaPagoSelected {
    return _formaPagoSelected;
  }
}
