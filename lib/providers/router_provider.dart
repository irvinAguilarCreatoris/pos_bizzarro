import 'package:flutter/material.dart';
import 'package:pos_bizzarro/pages/pages.dart';
import 'package:pos_bizzarro/router/app_routes.dart';

class RouterProvider extends ChangeNotifier {
  String _pageActive = '';
  bool _isExpanded = true;
  double _widthSidebar = 0;
  late Map<String, dynamic> _arguments;

  set setPageActive(String value) {
    _pageActive = value;
    notifyListeners();
  }

  String get getPageActive {
    return _pageActive;
  }

  set setWidthSidebar(double value) {
    _widthSidebar = value;
    notifyListeners();
  }

  double get getWidthSidebar {
    return _widthSidebar;
  }

  set setIsExpanded(bool value) {
    _isExpanded = value;
    notifyListeners();
  }

  bool get getIsExpanded {
    return _isExpanded;
  }

  Map<String, dynamic> get getArguments {
    return _arguments;
  }

  navigate(BuildContext context, String page,
      {Map<String, dynamic>? arguments}) {
    _arguments = arguments ?? {};

    final Widget Function(BuildContext c)? widget =
        AppRoutes.getAppRoutes()[page];
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
          pageBuilder: (context, animation1, animation2) =>
              widget != null ? widget(context) : const LoginPage(),
          transitionDuration: const Duration(milliseconds: 0)),
    );
  }
}
