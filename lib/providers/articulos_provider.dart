import 'package:flutter/material.dart';
import 'package:pos_bizzarro/models/models.dart';

class ArticulosProvider extends ChangeNotifier {
  final Map<String, ItemSelectedModel> _itemsSelected = {};
  int _changeCountValue = 0;
  double _totalPrice = 0;
  double _discount = 0;
  final double _taxes = 550;

  double get getTotalPrice {
    return _totalPrice;
  }
  double get getTaxes {
    return _taxes;
  }

  double get getDiscount {
    return _discount;
  }

  set setDiscount(double value) {
    _discount = value;
    notifyListeners();
  }

  void setItemSelected(String key, ItemSelectedModel model) {
    _itemsSelected[key] = model;
    _calculateTotalPrice();
    notifyListeners();
  }

  void deleteItem(String key) {
    if (_itemsSelected.containsKey(key)) {
      _itemsSelected.remove(key);
      _calculateTotalPrice();
    }
    notifyListeners();
  }

  set setChangeCountValue(int value) {
    _changeCountValue = value;
    notifyListeners();
  }

  int get getChangeCountValue {
    return _changeCountValue;
  }

  Map<String, ItemSelectedModel> get getItemsSelected {
    return _itemsSelected;
  }

  void _calculateTotalPrice() {
    _totalPrice = 0;
    // Iterar sobre las entradas del Map
    for (var entrada in _itemsSelected.entries) {
      ItemSelectedModel valor = entrada.value;

      _totalPrice = _totalPrice + (valor.price * valor.count);
    }
  }
}
