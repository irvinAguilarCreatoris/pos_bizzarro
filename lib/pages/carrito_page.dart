import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/card_container_component.dart';
import 'package:pos_bizzarro/components/card_pago_component.dart';
import 'package:pos_bizzarro/components/scaffold_component.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/forms/expansion_panel_form.dart';
import 'package:pos_bizzarro/forms/input_decorations.dart';
import 'package:pos_bizzarro/forms/numeric_input.dart';
import 'package:pos_bizzarro/models/models.dart';
import 'package:pos_bizzarro/providers/articulos_provider.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:pos_bizzarro/utils/utils.dart';
import 'package:provider/provider.dart';

class CarritoPage extends StatelessWidget {
  const CarritoPage({super.key});

  @override
  Widget build(BuildContext context) {
    final routerProvider = Provider.of<RouterProvider>(context);
    final articulosProvider = Provider.of<ArticulosProvider>(context);
    final Map<String, dynamic> args = routerProvider.getArguments;
    final size = MediaQuery.of(context).size;
    final widthPage = size.width - routerProvider.getWidthSidebar;
    final width1 = widthPage * 0.5;

    final String clienteNombre = args['clienteNombre'];
    double responsive1Expanded = 1335 - routerProvider.getWidthSidebar;
    double responsive1 = 1145 - routerProvider.getWidthSidebar;
    const double responsive4 = 730;

    Widget widgetPago = SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.all(30.0),
            child: TextComponent(
              'Pago',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          const SizedBox(height: 18),
          SizedBox(
            width: 500,
            child: CardPagoComponent(
              clienteNombre: clienteNombre,
              finalizarCompraEvent: () {
                routerProvider.navigate(context, 'pago',
                    arguments: {'clienteNombre': clienteNombre});
              },
            ),
          ),
        ],
      ),
    );

    return ScaffoldComponent(
      child: Padding(
        padding: const EdgeInsets.all(19.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SingleChildScrollView(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: widthPage > 862
                              ? widthPage - 150
                              : size.width > responsive4
                                  ? 860
                                  : 500,
                          child: TextComponent(
                              "Ventas > Nueva Venta > $clienteNombre > Listado Artículos > Finalizar compra"),
                        ),
                        size.width > responsive4
                            ? Row(
                                children: [
                                  Buttons.back(
                                    onPressed: () {
                                      routerProvider.navigate(
                                          context, 'articulos', arguments: {
                                        'clienteNombre': clienteNombre
                                      });
                                    },
                                  )
                                ],
                              )
                            : const SizedBox(),
                      ],
                    ),
                    size.width > responsive4
                        ? const SizedBox()
                        : Row(
                            children: [
                              Buttons.back(
                                onPressed: () {
                                  routerProvider.navigate(context, 'venta');
                                },
                              )
                            ],
                          ),
                    const SizedBox(height: 25),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: width1 > 538
                              ? width1
                              : ((size.width > 698) ? widthPage - 100 : 539),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(30.0),
                                child: TextComponent(
                                  'Resumen',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                              const SizedBox(
                                height: 19,
                              ),
                              CardContainerComponent(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 300,
                                      child: SingleChildScrollView(
                                        child: Wrap(
                                          runSpacing:
                                              1.0, // Ajusta el valor según tus necesidades
                                          spacing:
                                              0.0, // Ajusta el valor según tus necesidades
                                          alignment: WrapAlignment
                                              .center, // Centra las tarjetas en el eje principal
                                          children:
                                              List<Widget>.empty(growable: true)
                                                ..addAll(articulosProvider
                                                    .getItemsSelected.entries
                                                    .map((entry) {
                                                  final ItemSelectedModel
                                                      model = entry.value;
                                                  return CardResumen(
                                                    id: model.id,
                                                  );
                                                })),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.all(30.0),
                                child: TextComponent(
                                  'Descuentos',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                              CardContainerComponent(
                                  child: ExpansionPanelForm(
                                formRadio: false,
                                itemExpansionPanel:
                                    generateItemsDescuentos(articulosProvider),
                              )),
                              routerProvider.getIsExpanded
                                  ? ((widthPage > responsive1Expanded)
                                      ? const SizedBox()
                                      : widgetPago)
                                  : ((widthPage > responsive1)
                                      ? const SizedBox()
                                      : widgetPago)
                            ],
                          ),
                        ),
                        routerProvider.getIsExpanded
                            ? ((widthPage > responsive1Expanded)
                                ? widgetPago
                                : const SizedBox())
                            : ((widthPage > responsive1)
                                ? widgetPago
                                : const SizedBox()),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<ItemExpansionPanelModel> generateItemsDescuentos(
      ArticulosProvider articulosProvider) {
    double discount = 0;
    return [
      ItemExpansionPanelModel(
        headerValue: 'Puntos',
        expandedValue: const TextComponent('This is item Puntos'),
      ),
      ItemExpansionPanelModel(
        headerValue: 'Cupones',
        expandedValue: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const TextComponent('Folio:'),
                SizedBox(
                  width: 200,
                  child: TextField(
                    onChanged: (value) {},
                    decoration: InputDecorations.inputDecoration(
                      filled: true,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const TextComponent('Descuento:'),
                SizedBox(
                  width: 200,
                  child: TextField(
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        discount = double.parse(value);
                      }
                    },
                    decoration: InputDecorations.inputDecoration(
                      filled: true,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              width: 100,
              child: Buttons.buttonShape(
                  labelText: 'Aplicar',
                  onPressed: () {
                    articulosProvider.setDiscount = discount;
                  },
                  type: 'primary'),
            )
          ],
        ),
      ),
      ItemExpansionPanelModel(
        headerValue: 'Certificados (Descuento agregado)',
        expandedValue: const TextComponent('This is item Certificados'),
      ),
      ItemExpansionPanelModel(
        headerValue: 'Especial',
        expandedValue: const TextComponent('This is item Especial'),
      )
    ];
  }

  Widget setImageBank(String bank) {
    return SizedBox(
      width: 200,
      child: Column(
        children: [
          Image.asset(
            'assets/Image.png', // Asegúrate de ajustar la ruta de la imagen
            width: 60, // Ancho deseado
            height: 60, // Alto deseado
            fit: BoxFit.cover, // Ajusta la imagen al tamaño especificado
          ),
          const SizedBox(height: 10), // Espaciador vertical
          TextComponent(
            bank,
            style: const TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}

class CardResumen extends StatefulWidget {
  final int id;
  const CardResumen({super.key, required this.id});

  @override
  State<CardResumen> createState() => _CardResumenState();
}

class _CardResumenState extends State<CardResumen> {
  @override
  Widget build(BuildContext context) {
    final String title =
        '${widget.id} Argolla matrimonio dos piezas oro amarillo 14K FONA-60 (SKU)';
    String model = 'SKU FONA-60';
    String itemKey = 'item${widget.id}';
    final articulosProvider = Provider.of<ArticulosProvider>(context);
    ItemSelectedModel? articulo = articulosProvider.getItemsSelected[itemKey];
    double price = 0;

    if (articulo != null) {
      price = articulo.price;
    }

    //return Text('Mollit eu sit qui do fugiat tempor nisi occaecat.Commodo eiusmod mollit est esse enim nisi fugiat.Excepteur ex ipsum nulla excepteur anim aute dolore voluptate.');

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 6,
              child: Row(
                children: [
                  SizedBox(
                    width: 50.0,
                    child: Image.asset('assets/Image.png'),
                  ),
                  Expanded(
                    child: Utils.adjustableTextComponent(
                        context: context,
                        firtsTextBold: true,
                        texts: ['$title\n', 'Modelo:$model']),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: SizedBox(
                  width: 100,
                  child: NumericInput(
                    itemKey: itemKey,
                  )),
            ),
            Expanded(
              flex: 2,
              child: Column(
                children: [
                  Center(
                    child: IconButton(
                        onPressed: () {
                          setState(() {
                            articulosProvider.deleteItem('item${widget.id}');
                          });
                        },
                        icon: const Icon(
                          Icons.delete_outline_outlined,
                          color: Colors.deepPurple,
                        )),
                  ),
                  Utils.adjustableTextComponent(
                      context: context,
                      width: 100,
                      firtsTextBold: true,
                      texts: ['\$${Utils.currencyFormatToString(price)}']),
                ],
              ),
            ),
          ],
        ),
        const Divider(
          height: 30,
        ),
      ],
    );
  }
}
