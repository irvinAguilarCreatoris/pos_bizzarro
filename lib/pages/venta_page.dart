import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/components.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/forms/input_decorations.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:provider/provider.dart';

class VentaPage extends StatelessWidget {
  const VentaPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const VentaSFPage();
  }
}

class VentaSFPage extends StatefulWidget {
  const VentaSFPage({super.key});

  @override
  State<VentaSFPage> createState() => _VentaSFPageState();
}

class _VentaSFPageState extends State<VentaSFPage> {
  final TextEditingController _searchController = TextEditingController();
  List<String> _searchResults = [];

  @override
  Widget build(BuildContext context) {
    final routerProvider = Provider.of<RouterProvider>(context);
    final size = MediaQuery.of(context).size;
    final double widthPage = size.width - routerProvider.getWidthSidebar - 300;
    final btnAdd = Buttons.addClient(
      size: size,
      type: 'primary',
      onPressed: () {
        //if (!loginForm.isValidForm()) return;
        routerProvider.navigate(context, 'home');
      },
    );
    return ScaffoldComponent(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(19.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: SizedBox(
                    child: TextComponent("Ventas > Nueva Venta"),
                  ),
                ),
                SizedBox(
                  width: size.width < 600 ? 20 : 0,
                ),
                size.width < 600
                    ? Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: SizedBox(width: 200, child: btnAdd),
                      )
                    : const SizedBox(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: routerProvider.getIsExpanded
                          ? ((size.width > 800) ? widthPage : 300)
                          : ((size.width >= 600) ? widthPage : ((size.width > 480) ?  widthPage + 250 : 300)),
                      child: Column(
                        crossAxisAlignment:
                            CrossAxisAlignment.stretch, // Ajusta a lo ancho
                        children: [
                          TextField(
                            controller: _searchController,
                            onChanged: (value) {
                              // Lógica de búsqueda aquí
                              updateSearchResults(value);
                            },
                            decoration: InputDecorations.inputDecoration(
                              labelText: 'Buscar',
                              filled: true,
                              hintText: 'Ingrese su búsqueda',
                              suffixIcon: const Icon(Icons.search),
                            ),
                          ),
                          SizedBox(
                            height: size.height - 200,
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: _searchResults.length,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    // Manejar el evento de clic aquí
                                    routerProvider.navigate(context, 'cliente',
                                        arguments: {
                                          'clienteNombre': _searchResults[index]
                                        });
                                    // Puedes realizar otras acciones según sea necesario
                                  },
                                  child: Container(
                                    color: Colors.white,
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: const BoxDecoration(
                                              border: Border(
                                                  left: BorderSide(
                                                      color: Colors.deepPurple,
                                                      width: 3))),
                                          child: ListTile(
                                            title: TextComponent(
                                                _searchResults[index]),
                                          ),
                                        ),
                                        const Divider()
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    size.width >= 600 ? SizedBox(
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: btnAdd,
                          ),
                        ],
                      ),
                    ): const SizedBox(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void updateSearchResults(String query) {
    // Lógica para obtener resultados de búsqueda según la query
    // Aquí puedes llamar a tu fuente de datos, filtrar y actualizar _searchResults
    // Por ahora, simplemente demostraremos una búsqueda en una lista de ejemplo
    List<String> allItems = [
      'Juan García',
      'María Rodríguez',
      'Carlos López',
      'Ana Martínez',
      'Pedro Pérez',
      'Laura González',
      'Miguel Sánchez',
      'Isabel Ramírez',
      'Francisco Díaz',
      'Paula Herrera',
      'Alejandro Torres',
      'Carolina Flores',
      'Javier Cruz',
      'Marta Jiménez',
      'Raúl Romero',
      'Beatriz Álvarez',
      'Alberto Vargas',
      'Clara Medina',
      'Sergio Castro',
      'Elena Ruiz',
      'Andrés Gutiérrez',
      'Silvia Ortiz',
      'Diego Mendoza',
      'Natalia Guerrero',
      'Víctor Ramos',
      'Lucía Molina',
      'Gabriel Castro',
      'Adriana Silva',
      'Daniel Herrera',
      'Carolina Castro',
      'José Martínez',
      'Patricia López',
      'Luis Sánchez',
      'Marina Rodríguez',
      'Fernando Pérez',
      'Laura González',
      'Raúl Ramírez',
      'María Díaz',
      'Francisco Torres',
      'Paula Herrera',
      'Alejandro Vargas',
      'Clara Jiménez',
      'Javier Cruz',
      'Isabel Romero',
      'Miguel Álvarez',
      'Elena Medina',
      'Alberto Sánchez',
      'Beatriz Herrera',
      'Sergio Torres',
      'Marta Castro',
      'Andrés Molina',
      'Natalia Silva',
      'Gabriel Ramírez',
      'Silvia Ortiz',
      'Diego Mendoza',
      'Adriana Guerrero',
      'Víctor Herrera',
      'Lucía López',
      'Daniel Pérez',
      'Carolina Ramos',
      'José Rodríguez',
      'Patricia Díaz',
      'Luis Martínez',
      'Marina Sánchez',
      'Fernando Pérez',
      'Laura Gutiérrez',
      'Raúl Ramírez',
      'María Herrera',
      'Francisco Vargas',
      'Paula Torres',
      'Alejandro Jiménez',
      'Clara Cruz',
      'Javier Silva',
      'Isabel Mendoza',
      'Miguel Ramos',
      'Elena Castro',
      'Alberto Molina',
      'Beatriz Ortiz',
      'Sergio Herrera',
      'Marta Torres',
      'Andrés Ramírez',
      'Silvia Medina',
      'Diego Sánchez',
      'Natalia López',
      'Gabriel Pérez',
      'Lucía Rodríguez',
      'Daniel Díaz',
      'Carolina Martínez',
      'José Torres',
      'Patricia Castro',
      'Luis Silva',
      'Marina Herrera',
      'Fernando Pérez',
      'Laura Gutiérrez',
      'Raúl Ramírez',
      'María Herrera',
      'Francisco Vargas',
      'Paula Torres',
      'Alejandro Jiménez',
      'Clara Cruz'
    ];
    List<String> filteredItems = allItems
        .where((item) => item.toLowerCase().contains(query.toLowerCase()))
        .toList();

    setState(() {
      _searchResults = filteredItems;
    });
  }
}
