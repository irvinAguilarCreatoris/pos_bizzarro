import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/components.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:pos_bizzarro/utils/utils.dart';
import 'package:provider/provider.dart';

class ClienteDetallePage extends StatelessWidget {
  const ClienteDetallePage({super.key});

  @override
  Widget build(BuildContext context) {
    final routerProvider = Provider.of<RouterProvider>(context);
    final Map<String, dynamic> args = routerProvider.getArguments;
    final size = MediaQuery.of(context).size;
    final widhtPage = size.width - routerProvider.getWidthSidebar - 50;

    const double responsive1 = 1135;
    const double responsive2 = 960;
    double responsive3 = routerProvider.getIsExpanded ? 902 : 795;
    double responsive4 = routerProvider.getIsExpanded ? 830 : 730;
    const double responsive5 = 660;

    final String clienteNombre = args['clienteNombre'];

    void addItems() {
      routerProvider.navigate(context, 'articulos',
          arguments: {'clienteNombre': clienteNombre});
    }

    return ScaffoldComponent(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SingleChildScrollView(
                child: Padding(
          padding: const EdgeInsets.all(19.0),
          child: Row(
            children: [
              Column(
                children: [
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 300),
                    width: widhtPage > 400 ? widhtPage : 400,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextComponent("Ventas > Nueva Venta > $clienteNombre"),
                        size.width > responsive4
                            ? Row(
                                children: [
                                  Buttons.back(
                                    onPressed: () {
                                      routerProvider.navigate(context, 'venta');
                                    },
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Buttons.buttonShape(
                                      labelText: size.width > responsive3
                                          ? 'Añadir Articulos'
                                          : 'Añadir',
                                      onPressed: addItems,
                                      type: 'primary',
                                      prefixIcon:
                                          Icons.add_shopping_cart_outlined,
                                      verticalPadding: 8),
                                ],
                              )
                            : const SizedBox(),
                      ],
                    ),
                  ),
                  size.width > responsive4
                      ? const SizedBox()
                      : Row(
                          children: [
                            Buttons.back(
                              onPressed: () {
                                routerProvider.navigate(context, 'venta');
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Buttons.buttonShape(
                                labelText: 'Añadir Articulos',
                                onPressed: addItems,
                                type: 'primary',
                                prefixIcon: Icons.add_shopping_cart_outlined,
                                verticalPadding: 8),
                          ],
                        ),
                  const SizedBox(height: 25),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 300),
                    width: widhtPage > 1000 ? 1000 : widhtPage > 416 ? widhtPage : 450,
                    child: CardContainerComponent(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            size.width < responsive5
                                ? const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      TextComponent('Fecha creación'),
                                      TextComponent(
                                        '03/10/2023',
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ],
                                  )
                                : const SizedBox(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    TextComponent(
                                      clienteNombre,
                                      style: const TextStyle(fontSize: 24),
                                    ),
                                    const SizedBox(width: 8,),
                                    const Icon(Icons.edit_outlined, color: Colors.deepPurple,)
                                  ],
                                ),
                                size.width > responsive5
                                    ? const TextComponent('Fecha creación')
                                    : const SizedBox(),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const TextComponent(
                                  'Membresía #5548622895',
                                  style: TextStyle(fontSize: 14),
                                ),
                                size.width > responsive5
                                    ? const TextComponent('03/10/2023')
                                    : const SizedBox(),
                              ],
                            ),
                            const SizedBox(height: 25),
                            const TextComponent(
                              'Datos Fiscales',
                              style: TextStyle(
                                  fontSize: 16, color: Colors.deepPurple),
                            ),
                            const Divider(),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['RFC: ', 'RFC123456']),
                                size.width > responsive3
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: [
                                            'Tipo de persona: ',
                                            'Jurídica'
                                          ])
                                    : const SizedBox(),
                                size.width > responsive2
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: [
                                            'Regimen fiscal: ',
                                            'Incididunt.'
                                          ])
                                    : const SizedBox(),
                                size.width > responsive1
                                    ? Utils.adjustableTextComponent(
                                        context: context, texts: [''])
                                    : const SizedBox(),
                              ],
                            ),
                            size.width > responsive2
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive2
                                ? const SizedBox()
                                : Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Utils.adjustableTextComponent(
                                          context: context,
                                          firtsTextBold: true,
                                          texts: [
                                            'Regimen fiscal: ',
                                            'Incididunt.'
                                          ])
                                    ],
                                  ),
                            size.width > responsive3
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive3
                                ? const SizedBox()
                                : Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Utils.adjustableTextComponent(
                                          context: context,
                                          firtsTextBold: true,
                                          texts: [
                                            'Tipo de persona: ',
                                            'Jurídica'
                                          ])
                                    ],
                                  ),
                            const SizedBox(height: 8),
                            Row(
                              children: [
                                Row(
                                  children: [
                                    Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        width: 212,
                                        texts: [
                                          'Uso CFDI: ',
                                          'Et eiusmod velit veniam.'
                                        ]),
                                  ],
                                ),
                              ],
                            ),
                            const SizedBox(height: 25),
                            const TextComponent(
                              'Dirección',
                              style: TextStyle(
                                  fontSize: 16, color: Colors.deepPurple),
                            ),
                            const Divider(),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['Pais: ', 'México']),
                                size.width > responsive3
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: ['Estado: ', 'Non minim.'])
                                    : const SizedBox(),
                                size.width > responsive2
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: ['#Int: ', '000'])
                                    : const SizedBox(),
                                size.width > responsive1
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: ['Código Postal: ', '1010'])
                                    : const SizedBox(),
                              ],
                            ),
                            size.width > responsive1
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive1
                                ? const SizedBox()
                                : Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      size.width > responsive3
                                          ? size.width > responsive2
                                              ? size.width > responsive1
                                                  ? const SizedBox()
                                                  : Utils
                                                      .adjustableTextComponent(
                                                          context: context,
                                                          firtsTextBold: true,
                                                          texts: [
                                                          'Código Postal: ',
                                                          '1010'
                                                        ])
                                              : Utils.adjustableTextComponent(
                                                  context: context,
                                                  firtsTextBold: true,
                                                  texts: ['#Int: ', '000'])
                                          : Utils.adjustableTextComponent(
                                              context: context,
                                              firtsTextBold: true,
                                              texts: [
                                                  'Estado: ',
                                                  'Non minim.'
                                                ]),
                                      size.width > responsive3
                                          ? size.width > responsive2
                                              ? const SizedBox()
                                              : Utils.adjustableTextComponent(
                                                  context: context,
                                                  firtsTextBold: true,
                                                  texts: [
                                                      'Código Postal: ',
                                                      '1010'
                                                    ])
                                          : const SizedBox(),
                                      const SizedBox(),
                                      const SizedBox()
                                    ],
                                  ),
                            size.width > responsive3
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive3
                                ? const SizedBox()
                                : Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['#Int: ', '000']),
                            size.width > responsive3
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive3
                                ? const SizedBox()
                                : Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['Código Postal: ', '1010']),
                            const SizedBox(height: 8),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['Ciudad: ', 'Et minim.']),
                                size.width > responsive3
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: [
                                            'Calle: ',
                                            'Aliqua ex cupidatat consequat.'
                                          ])
                                    : const SizedBox(),
                                size.width > responsive2
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: ['#Ext: ', '000'])
                                    : const SizedBox(),
                                size.width > responsive1
                                    ? Utils.adjustableTextComponent(
                                        context: context, texts: [''])
                                    : const SizedBox(),
                              ],
                            ),
                            size.width > responsive3
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive3
                                ? const SizedBox()
                                : Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: [
                                        'Calle: ',
                                        'Aliqua ex cupidatat consequat.'
                                      ]),
                            size.width > responsive2
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive2
                                ? const SizedBox()
                                : Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['#Ext: ', '000']),
                            const SizedBox(height: 25),
                            const TextComponent(
                              'Beneficios',
                              style: TextStyle(
                                  fontSize: 16, color: Colors.deepPurple),
                            ),
                            const Divider(),
                            const SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['Cumpleaños: ', '20-12-1982']),
                                size.width > responsive2
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: ['Puntos: ', '40'])
                                    : const SizedBox(),
                                size.width > responsive2
                                    ? Utils.adjustableTextComponent(
                                        context: context,
                                        firtsTextBold: true,
                                        texts: ['Cupones: ', '4'])
                                    : const SizedBox(),
                                size.width > responsive1
                                    ? Utils.adjustableTextComponent(
                                        context: context, texts: [''])
                                    : const SizedBox(),
                              ],
                            ),
                            size.width > responsive3
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive3
                                ? const SizedBox()
                                : Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['Puntos: ', '40']),
                            size.width > responsive2
                                ? const SizedBox()
                                : const SizedBox(height: 8),
                            size.width > responsive2
                                ? const SizedBox()
                                : Utils.adjustableTextComponent(
                                    context: context,
                                    firtsTextBold: true,
                                    texts: ['Cupones: ', '4']),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
                ),
              ),
        ));
  }
}
