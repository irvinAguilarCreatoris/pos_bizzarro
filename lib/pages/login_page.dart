import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/forms/input_decorations.dart';
import 'package:pos_bizzarro/providers/login_form_provider.dart';
import 'package:pos_bizzarro/components/components.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:pos_bizzarro/utils/utils.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        body: AuthBackgroundComponent(
            child: Row(
      children: [
        SizedBox(
          width: size.width > 980 ? size.width * 0.5 : 0,
        ),
        SizedBox(
          width: size.width > 980 ? size.width * 0.5 : size.width,
          child: Center(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Column(
                children: [
                  SizedBox(
                    height: size.height * 0.25,
                  ),
                  Container(
                    constraints: const BoxConstraints(
                      minWidth: 370, // Establece el ancho mínimo deseado
                      maxWidth: 600, // Establece el ancho máximo deseado
                    ),
                    child: CardContainerComponent(
                        child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        const TextComponent(
                          'Inicio de sesión',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        ChangeNotifierProvider(
                          create: (context) => LoginFormProvider(),
                          child: const _LoginForm(
                            isAdmin: false,
                          ),
                        )
                      ],
                    )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    )));
  }
}

class _LoginForm extends StatefulWidget {
  final bool isAdmin;

  const _LoginForm({required this.isAdmin});

  @override
  State<_LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<_LoginForm> {
  late bool isAdmin;
  _LoginFormState();
  bool isPasswordVisible = false;

  void togglePasswordVisibility() {
    setState(() {
      isPasswordVisible = !isPasswordVisible;
    });
  }

  @override
  void initState() {
    super.initState();
    isAdmin = widget.isAdmin;
  }

  @override
  Widget build(BuildContext context) {
    final routerProvider = Provider.of<RouterProvider>(context);
    final loginForm = Provider.of<LoginFormProvider>(context);
    final size = MediaQuery.of(context).size;

    return Form(
        key: loginForm.formkey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            TextFormField(
              autocorrect: false,
              keyboardType: TextInputType.text,
              decoration: InputDecorations.inputDecoration(
                labelText: 'Usuario',
              ),
              onChanged: (value) => loginForm.email = value,
              validator: (value) {
                return (value != null && value.length >= 6)
                    ? null
                    : 'Usuario no valido';
              },
            ),
            const SizedBox(
              height: 30,
            ),
            TextFormField(
              autocorrect: false,
              obscureText: !isPasswordVisible,
              keyboardType: TextInputType.text,
              decoration: InputDecorations.inputDecoration(
                  hintText: '*****',
                  labelText: 'Clave',
                  isPassword: true,
                  isPasswordVisible: isPasswordVisible,
                  togglePassword: togglePasswordVisibility),
              onChanged: (value) => loginForm.password = value,
              validator: (value) {
                return (value != null && value.length >= 6)
                    ? null
                    : 'La contraseña debe de ser de 6 caracteres';
              },
            ),
            const SizedBox(
              height: 30,
            ),
            Buttons.buttonShape(
                labelText: 'INGRESAR',
                type: 'primary',
                minWidth: double.infinity,
                horizontalPadding: 80,
                verticalPadding: 15,
                onPressed: () {
                  //if (!loginForm.isValidForm()) return;

                  routerProvider.navigate(context, 'home');
                  routerProvider.setPageActive = 'Inicio';
                  routerProvider.setWidthSidebar = 260;
                }),
            const SizedBox(
              height: 25,
            ),
            const TextComponent(
              '¿Olvidaste tu contraseña?',
              style: TextStyle(
                color: Colors.deepPurple,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            isAdmin
                ? const SizedBox()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const TextComponent(
                        'Inicio de sesión como ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      MouseRegion(
                        cursor: SystemMouseCursors.click,
                        child: GestureDetector(
                          onTap: () {
                            Utils.showAdminDialog(
                                context: context,
                                title: 'Inicio de sesión como administrador',
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: SizedBox(
                                    width: size.width > 370 ? 370 : 200,
                                    child: Column(
                                      children: [
                                        ChangeNotifierProvider(
                                          create: (context) =>
                                              LoginFormProvider(),
                                          child: const _LoginForm(
                                            isAdmin: true,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ));
                          },
                          child: const TextComponent(
                            'administrador',
                            style: TextStyle(
                              color: Colors.deepPurple,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
          ],
        ));
  }
}
