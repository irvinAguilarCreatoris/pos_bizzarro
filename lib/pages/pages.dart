export 'package:pos_bizzarro/pages/pago_page.dart';
export 'package:pos_bizzarro/pages/venta_page.dart';
export 'package:pos_bizzarro/pages/carrito_page.dart';
export 'package:pos_bizzarro/pages/cliente_detalle_page.dart';
export 'package:pos_bizzarro/pages/articulos_page.dart';
export 'package:pos_bizzarro/pages/home_page.dart';
export 'package:pos_bizzarro/pages/login_page.dart';