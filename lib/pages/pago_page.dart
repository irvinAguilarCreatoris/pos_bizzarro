import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/card_container_component.dart';
import 'package:pos_bizzarro/components/card_pago_component.dart';
import 'package:pos_bizzarro/components/scaffold_component.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/forms/input_decorations.dart';
import 'package:pos_bizzarro/models/forma_pago_selected_model.dart';
import 'package:pos_bizzarro/providers/articulos_provider.dart';
import 'package:pos_bizzarro/providers/pagos_provider.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:pos_bizzarro/utils/utils.dart';
import 'package:provider/provider.dart';

class PagoPage extends StatelessWidget {
  const PagoPage({super.key});

  @override
  Widget build(BuildContext context) {
    final routerProvider = Provider.of<RouterProvider>(context);
    final pagosProvider = Provider.of<PagosProvider>(context);
    final articulosProvider = Provider.of<ArticulosProvider>(context);
    final Map<String, dynamic> args = routerProvider.getArguments;
    final size = MediaQuery.of(context).size;
    double widthPage = size.width - routerProvider.getWidthSidebar;

    final String clienteNombre = args['clienteNombre'];
    const double responsive4 = 730;

    double totalPagar = articulosProvider.getTotalPrice + articulosProvider.getTaxes - articulosProvider.getDiscount;
    double saldoPendiente = totalPagar - pagosProvider.getFormaPagoSelected.entries.map((entry) => entry.value.importe).toList().fold(0, (previousValue, element) => previousValue + element);

    Widget cardCliente = SizedBox(
      width: 400,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 100,
          ),
          CardPagoComponent(
            clienteNombre: clienteNombre,
          ),
        ],
      ),
    );

    return ScaffoldComponent(
      child: Padding(
        padding: const EdgeInsets.all(19.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: widthPage > 862
                      ? widthPage - 50
                      : size.width > responsive4
                          ? 860
                          : 500,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextComponent(
                          "Ventas > Nueva Venta > $clienteNombre > Listado Artículos > Finalizar compra"),
                      size.width > responsive4
                          ? Row(
                              children: [
                                Buttons.back(
                                  onPressed: () {
                                    routerProvider.navigate(
                                        context, 'articulos', arguments: {
                                      'clienteNombre': clienteNombre
                                    });
                                  },
                                )
                              ],
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
                size.width > responsive4
                    ? const SizedBox()
                    : Row(
                        children: [
                          Buttons.back(
                            onPressed: () {
                              routerProvider.navigate(context, 'venta');
                            },
                          )
                        ],
                      ),
                const SizedBox(height: 25),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          widthPage > 1187 ? const SizedBox() : cardCliente,
                          const Padding(
                            padding: EdgeInsets.all(30.0),
                            child: TextComponent(
                              'Seleccione sus métodos de pago',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                          ),
                          const SizedBox(
                            height: 19,
                          ),
                          SizedBox(
                            width: 750,
                            child: CardContainerComponent(
                                child: Column(
                              children: [
                                const Wrap(
                                  runSpacing:
                                      1.0, // Ajusta el valor según tus necesidades
                                  spacing:
                                      0.0, // Ajusta el valor según tus necesidades
                                  alignment: WrapAlignment
                                      .center, // Centra las tarjetas en el eje principal
                                  children: [
                                    BankImageWidget(
                                      bank: 'Efectivo',
                                      pathImage: 'assets/Fectivo_icon.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'Santander',
                                      pathImage: 'assets/Santander_logo.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'BBVA',
                                      pathImage: 'assets/BBVA_logo.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'Banamex',
                                      pathImage: 'assets/Banamex_logo.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'American Express',
                                      pathImage:
                                          'assets/AmericanExpress_logo.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'BBVA Interfaz',
                                      pathImage: 'assets/BBVA_logo.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'American Express Interfaz',
                                      pathImage:
                                          'assets/AmericanExpress_logo.png',
                                    ),
                                    BankImageWidget(
                                      bank: 'Otros',
                                      pathImage: 'assets/Icon_Otros.png',
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  child: Column(
                                    children: [
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      const Divider(),
                                      Padding(
                                        padding: const EdgeInsets.all(25.0),
                                        child: Column(
                                          children: [
                                            pagosProvider.getMetodoPagoValue
                                                    .contains('Interfaz')
                                                ? Column(
                                                    children: [
                                                      Center(
                                                        child: TextComponent(
                                                          'Promociones - ${pagosProvider.getMetodoPagoValue}',
                                                          style: const TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 25,
                                                      ),
                                                      const RadioBtn(),
                                                      const SizedBox(
                                                        height: 25,
                                                      ),
                                                    ],
                                                  )
                                                : const SizedBox(),
                                            pagosProvider
                                                    .getMetodoPagoValue.isEmpty
                                                ? const SizedBox()
                                                : TextFormField(
                                                    autocorrect: false,
                                                    keyboardType:
                                                        TextInputType.text,
                                                    decoration: InputDecorations
                                                        .inputDecoration(
                                                            hintText:
                                                                'Ingrese monto',
                                                            labelText: 'Monto',
                                                            suffixIcon: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .only(
                                                                      right: 5),
                                                              child: SizedBox(
                                                                width: 115,
                                                                child: Buttons
                                                                    .buttonShape(
                                                                        labelText:
                                                                            'Agregar',
                                                                        onPressed:
                                                                            () {
                                                                          pagosProvider.setItemSelected(
                                                                              pagosProvider.getMetodoPagoValue,
                                                                              FormaPagoSelectedModel(id: 0, formaPago: pagosProvider.getMetodoPagoValue, referencia: pagosProvider.getMetodoPagoValue == 'Efectivo' ? '' : '00000008', importe: pagosProvider.getMountToPay));
                                                                        },
                                                                        type:
                                                                            'primary'),
                                                              ),
                                                            )),
                                                    onChanged: (value) {
                                                      if (value.isNotEmpty) {
                                                        pagosProvider.setMountToPay =
                                                            double.parse(value);
                                                      }
                                                    },
                                                  ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                          ),
                        ],
                      ),
                    ),
                    widthPage > 1187 ? cardCliente : const SizedBox()
                  ],
                ),
                const SizedBox(
                  height: 40,
                ),
                pagosProvider.getFormaPagoSelected.entries.isEmpty
                    ? const SizedBox()
                    : SizedBox(
                        width: widthPage,
                        child: CardContainerComponent(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Utils.adjustableTextComponent(
                                      context: context,
                                      width: 300,
                                      texts: ['Forma de pago'],
                                      firtsTextBold: true),
                                  Utils.adjustableTextComponent(
                                      context: context,
                                      width: 100,
                                      texts: ['Referencia'],
                                      firtsTextBold: true),
                                  Utils.adjustableTextComponent(
                                      context: context,
                                      width: 100,
                                      texts: ['Importe pagado'],
                                      firtsTextBold: true),
                                  Utils.adjustableTextComponent(
                                      context: context,
                                      width: 100,
                                      texts: [''],
                                      firtsTextBold: true),
                                ],
                              ),
                              const Divider(
                                height: 30,
                              ),
                              SingleChildScrollView(
                                child: Wrap(
                                  runSpacing:
                                      1.0, // Ajusta el valor según tus necesidades
                                  spacing:
                                      0.0, // Ajusta el valor según tus necesidades
                                  alignment: WrapAlignment
                                      .center, // Centra las tarjetas en el eje principal
                                  children: List<Widget>.empty(growable: true)
                                    ..addAll(pagosProvider
                                        .getFormaPagoSelected.entries
                                        .map((entry) {
                                      final FormaPagoSelectedModel model =
                                          entry.value;
                                      return Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              SizedBox(
                                                width: 300,
                                                child: TextComponent(
                                                    model.formaPago),
                                              ),
                                              SizedBox(
                                                  width: 100,
                                                  child: TextComponent(
                                                      model.referencia)),
                                              Utils.adjustableTextComponent(
                                                  context: context,
                                                  width: 100,
                                                  firtsTextBold: true,
                                                  texts: [
                                                    '\$${Utils.currencyFormatToString(model.importe)}'
                                                  ]),
                                              IconButton(
                                                onPressed: () {
                                                  pagosProvider.deleteItem(model.formaPago);
                                                },
                                                icon: const Icon(
                                                  Icons.delete_outline_outlined,
                                                  color: Colors.deepPurple,
                                                ),
                                              )
                                            ],
                                          ),
                                          const Divider()
                                        ],
                                      );
                                    })),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: 500,
                  child: CardContainerComponent(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Utils.adjustableTextComponent(
                                context: context, texts: ['Total a pagar']),
                            Utils.adjustableTextComponent(
                                context: context,
                                texts: [
                                  '\$${Utils.currencyFormatToString(totalPagar)}'
                                ],
                                firtsTextBold: true),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Utils.adjustableTextComponent(
                                context: context, texts: ['Saldo pendiente']),
                            Utils.adjustableTextComponent(
                                context: context,
                                texts: ['\$${Utils.currencyFormatToString(saldoPendiente < 0 ? 0 : saldoPendiente)}'],
                                firtsTextBold: true),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Utils.adjustableTextComponent(
                                context: context, texts: ['Cambio']),
                            Utils.adjustableTextComponent(
                                context: context,
                                texts: ['\$${Utils.currencyFormatToString(saldoPendiente < 0 ? -1 * saldoPendiente : 0)}'],
                                firtsTextBold: true),
                          ],
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        Buttons.buttonShape(
                            labelText: 'Pagar ahora',
                            onPressed: saldoPendiente <= 0 ? () {
                              Utils.showAdminDialog(
                                  context: context,
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        width: 200,
                                        child: Buttons.buttonShape(
                                            labelText: 'Imprimir Ticket',
                                            onPressed: () {},
                                            type: 'primary',
                                            verticalPadding: 10),
                                      )
                                    ],
                                  ),
                                  title: 'Pago exitoso');
                            } : null,
                            type: 'primary')
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class BankImageWidget extends StatefulWidget {
  final String bank;
  final String pathImage;

  const BankImageWidget(
      {super.key, required this.bank, required this.pathImage});

  @override
  State<BankImageWidget> createState() => _BankImageWidgetState();
}

class _BankImageWidgetState extends State<BankImageWidget> {
  bool _isClicked = false;
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    final pagosProvider = Provider.of<PagosProvider>(context);
    final String bank = pagosProvider.getMetodoPagoValue;
    _isClicked = bank == widget.bank;
    return MouseRegion(
      onHover: (_) {
        setState(() {
          _isHovered = true;
        });
      },
      onExit: (_) {
        setState(() {
          _isHovered = false;
        });
      },
      child: Container(
        width: 145,
        decoration: BoxDecoration(
          color: _isHovered ? Colors.blue.withOpacity(0.1) : null,
          borderRadius: BorderRadius.circular(5),
          border: _isClicked
              ? Border.all(width: 2, color: Colors.deepPurple)
              : null,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              setState(() {
                _isClicked = !_isClicked;
                if (_isClicked) {
                  pagosProvider.setMetodoPagoValue = widget.bank;
                }
                _isClicked = _isClicked &&
                    pagosProvider.getMetodoPagoValue == widget.bank;
              });
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  widget.pathImage, // Asegúrate de ajustar la ruta de la imagen
                  width: 60, // Ancho deseado
                  height: 60, // Alto deseado
                  fit:
                      BoxFit.contain, // Ajusta la imagen al tamaño especificado
                ),
                const SizedBox(height: 10), // Espaciador vertical
                TextComponent(
                  widget.bank,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RadioBtn extends StatefulWidget {
  const RadioBtn({super.key});

  @override
  State<RadioBtn> createState() => _RadioBtnState();
}

class _RadioBtnState extends State<RadioBtn> {
  int _selectedRadio = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Wrap(
          runSpacing: 1.0, // Ajusta el valor según tus necesidades
          spacing: 0.0, // Ajusta el valor según tus necesidades
          alignment:
              WrapAlignment.start, // Centra las tarjetas en el eje principal
          children: [
            SizedBox(
              width: 230,
              child: RadioListTile(
                title: const TextComponent('Interfaz'),
                value: 1,
                groupValue: _selectedRadio,
                onChanged: (value) {
                  setState(() {
                    _selectedRadio = value!;
                  });
                },
              ),
            ),
            SizedBox(
              width: 230,
              child: RadioListTile(
                title: const TextComponent('12 meses interfaz'),
                value: 2,
                groupValue: _selectedRadio,
                onChanged: (value) {
                  setState(() {
                    _selectedRadio = value!;
                  });
                },
              ),
            ),
            SizedBox(
              width: 230,
              child: RadioListTile(
                title: const TextComponent('9 meses interfaz'),
                value: 3,
                groupValue: _selectedRadio,
                onChanged: (value) {
                  setState(() {
                    _selectedRadio = value!;
                  });
                },
              ),
            ),
            SizedBox(
              width: 230,
              child: RadioListTile(
                title: const TextComponent('6 meses interfaz'),
                value: 4,
                groupValue: _selectedRadio,
                onChanged: (value) {
                  setState(() {
                    _selectedRadio = value!;
                  });
                },
              ),
            ),
            SizedBox(
              width: 230,
              child: RadioListTile(
                title: const TextComponent('Debito interfaz'),
                value: 5,
                groupValue: _selectedRadio,
                onChanged: (value) {
                  setState(() {
                    _selectedRadio = value!;
                  });
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
