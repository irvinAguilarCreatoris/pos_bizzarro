import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/components.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/forms/input_decorations.dart';
import 'package:pos_bizzarro/forms/numeric_input.dart';
import 'package:pos_bizzarro/forms/select_input.dart';
import 'package:pos_bizzarro/models/models.dart';
import 'package:pos_bizzarro/providers/articulos_provider.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:pos_bizzarro/utils/utils.dart';
import 'package:provider/provider.dart';

class ArticulosPage extends StatelessWidget {
  const ArticulosPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<ArticulosProvider>(
        builder: (context, articulosProvider, _) {
      final routerProvider = Provider.of<RouterProvider>(context);
      final articulosProvider = Provider.of<ArticulosProvider>(context);
      final Map<String, dynamic> args = routerProvider.getArguments;
      final size = MediaQuery.of(context).size;
      final widthPage = size.width - routerProvider.getWidthSidebar;
      double widthsearch = (widthPage - 450) / 2;

      final String clienteNombre = args['clienteNombre'];

      const double responsive3 = 900;
      const double responsive4 = 850;

      widthsearch = widthsearch > 250 ? widthsearch : 250;

      return ScaffoldComponent(
        child: Padding(
          padding: const EdgeInsets.all(19.0),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    width: widthPage > 862
                        ? widthPage - 115
                        : size.width > responsive4
                            ? 860
                            : 500,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          child: TextComponent(
                              "Ventas > Nueva Venta > $clienteNombre > Listado Artículos"),
                        ),
                        size.width > responsive4
                            ? Row(
                                children: [
                                  Buttons.back(
                                    onPressed: () {
                                      routerProvider.navigate(
                                        context,
                                        'cliente',
                                        arguments: {
                                          'clienteNombre': clienteNombre
                                        },
                                      );
                                    },
                                  ),
                                  const SizedBox(width: 10),
                                  Buttons.buttonShape(
                                    labelText: size.width > responsive3
                                        ? 'Finalizar compra'
                                        : 'Finalizar',
                                    onPressed: () {
                                      routerProvider.navigate(
                                          context, 'carrito', arguments: {
                                        'clienteNombre': clienteNombre
                                      });
                                    },
                                    type: 'primary',
                                    verticalPadding: 8,
                                  ),
                                ],
                              )
                            : const SizedBox(),
                      ],
                    ),
                  ),
                  size.width > responsive4
                      ? const SizedBox()
                      : Wrap(
                          runSpacing:
                              1.0, // Ajusta el valor según tus necesidades
                          spacing: 0.0, // Ajusta el valor según tus necesidades
                          alignment: WrapAlignment
                              .start, // Centra las tarjetas en el eje principal
                          children: [
                            SizedBox(
                              width: 200,
                              child: Buttons.back(
                                onPressed: () {
                                  routerProvider.navigate(context, 'venta');
                                },
                              ),
                            ),
                            const SizedBox(width: 10),
                            SizedBox(
                              width: 200,
                              child: Buttons.buttonShape(
                                labelText: 'Finalizar compra',
                                onPressed: () {
                                  routerProvider.navigate(context, 'carrito',
                                      arguments: {
                                        'clienteNombre': clienteNombre
                                      });
                                },
                                type: 'primary',
                                prefixIcon: Icons.add_shopping_cart_outlined,
                                verticalPadding: 8,
                              ),
                            ),
                          ],
                        ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width:
                            (widthPage - 400) > 300 ? (widthPage - 400) : 300,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.all(10.0),
                              child: TextComponent(
                                'Listado Artículos',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Wrap(
                              runSpacing:
                                  1.0, // Ajusta el valor según tus necesidades
                              spacing:
                                  0.0, // Ajusta el valor según tus necesidades
                              alignment: WrapAlignment
                                  .center, // Centra las tarjetas en el eje principal
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: AnimatedContainer(
                                    duration: const Duration(milliseconds: 300),
                                    width: widthsearch,
                                    child: TextField(
                                      onChanged: (value) {},
                                      decoration:
                                          InputDecorations.inputDecoration(
                                        labelText: 'Buscar',
                                        filled: true,
                                        hintText: 'Ingrese su búsqueda',
                                        suffixIcon: const Icon(Icons.search),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: AnimatedContainer(
                                    duration: const Duration(milliseconds: 300),
                                    width: widthsearch,
                                    child: const SelectInput(items: [
                                      'Anillos de Compromiso',
                                      'Argollas de Matrimonio',
                                      'Mujer',
                                      'Hombre',
                                      'Unisex',
                                      'Eco-Luxury',
                                      'Religiosa',
                                      'Relojes'
                                    ], selectedValue: 'Anillos de Compromiso'),
                                  ),
                                ),
                              ],
                            ),
                            Center(
                              child: Wrap(
                                runSpacing:
                                    1.0, // Ajusta el valor según tus necesidades
                                spacing:
                                    0.0, // Ajusta el valor según tus necesidades
                                alignment: WrapAlignment
                                    .center, // Centra las tarjetas en el eje principal
                                children: List.generate(
                                  30,
                                  (index) => SizedBox(
                                    width: 280,
                                    child: CardArticulo(id: index),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(25.0),
                            child: TextComponent(
                              'Resumen',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            width: 300,
                            child: CardContainerComponent(
                                child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const TextComponent('Subtotal:'),
                                    Utils.currencyFormat(
                                        articulosProvider.getTotalPrice)
                                  ],
                                ),
                                const Divider(),
                                Wrap(
                                  runSpacing:
                                      1.0, // Ajusta el valor según tus necesidades
                                  spacing:
                                      0.0, // Ajusta el valor según tus necesidades
                                  alignment: WrapAlignment
                                      .center, // Centra las tarjetas en el eje principal
                                  children: List<Widget>.empty(growable: true)
                                    ..addAll(articulosProvider
                                        .getItemsSelected.entries
                                        .map((entry) {
                                      final ItemSelectedModel model =
                                          entry.value;
                                      return SizedBox(
                                        width: 300,
                                        child: CardArticuloSelected(
                                          id: model.id,
                                        ),
                                      );
                                    })),
                                )
                              ],
                            )),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}

class CardArticulo extends StatefulWidget {
  final int id;
  const CardArticulo({super.key, required this.id});

  @override
  State<CardArticulo> createState() => _CardArticuloState();
}

class _CardArticuloState extends State<CardArticulo> {
  bool isButtonPressed = false;
  late int id;

  @override
  void initState() {
    super.initState();
    id = widget.id; // Asigna items aquí en el initState
  }

  @override
  Widget build(BuildContext context) {
    final articulosProvider = Provider.of<ArticulosProvider>(context);
    final String title =
        '$id Argolla matrimonio dos piezas oro amarillo 14K FONA-60 (SKU)';
    const String model = 'SKU FONA-60';
    isButtonPressed = isButtonPressed = articulosProvider.getItemsSelected.containsKey('item$id');
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: CardContainerComponent(
        widhtPadding: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 70.0,
                  child: Image.asset('assets/Image.png'),
                ),
              ],
            ),
            const SizedBox(height: 10),
            TextComponent(
              title,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            const TextComponent(
              'Modelo: $model',
              style: TextStyle(
                fontSize: 14,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                (isButtonPressed && articulosProvider.getItemsSelected.containsKey('item$id'))
                    ? Row(
                        children: [
                          const TextComponent(
                            'Añadido ',
                            style: TextStyle(
                                color: Colors.deepPurple,
                                fontWeight: FontWeight.bold),
                          ),
                          NumericInput(
                            itemKey: 'item$id',
                          ),
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  isButtonPressed = false;
                                  articulosProvider.deleteItem('item$id');
                                });
                              },
                              icon: const Icon(
                                Icons.delete_outline_outlined,
                                color: Colors.deepPurple,
                              ))
                        ],
                      )
                    : Buttons.buttonShape(
                        labelText: 'Añadir',
                        type: 'primary',
                        onPressed: () {
                          setState(() {
                            isButtonPressed = true;
                            articulosProvider.setItemSelected(
                                'item$id',
                                ItemSelectedModel(
                                    id: id,
                                    title: title,
                                    model: model,
                                    price: 2000,
                                    count: 1));
                            articulosProvider.setChangeCountValue = 1;
                          });
                        },
                      )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CardArticuloSelected extends StatefulWidget {
  final int id;
  const CardArticuloSelected({super.key, required this.id});

  @override
  State<CardArticuloSelected> createState() => _CardArticuloSelectedState();
}

class _CardArticuloSelectedState extends State<CardArticuloSelected> {
  bool isButtonPressed = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final articulosProvider = Provider.of<ArticulosProvider>(context);
    final String title =
        '${widget.id} Argolla matrimonio dos piezas oro amarillo 14K FONA-60 (SKU)';
    String model = 'SKU FONA-60';

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 60.0,
                child: Image.asset('assets/Image.png'),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  IconButton(
                      onPressed: () {
                        setState(() {
                          isButtonPressed = false;
                          articulosProvider.deleteItem('item${widget.id}');
                        });
                      },
                      icon: const Icon(
                        Icons.delete_outline_outlined,
                        color: Colors.deepPurple,
                      )),
                  NumericInput(
                    itemKey: 'item${widget.id}',
                  )
                ],
              )
            ],
          ),
          const SizedBox(height: 10),
          TextComponent(
            title,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
            overflow: TextOverflow
                .ellipsis, // Muestra tres puntos suspensivos al final del texto
            maxLines:
                1, // Define el número máximo de líneas que puede tener el texto
          ),
          const SizedBox(height: 5),
          TextComponent(
            'Modelo: $model',
            style: const TextStyle(
              fontSize: 14,
            ),
            overflow: TextOverflow
                .ellipsis, // Muestra tres puntos suspensivos al final del texto
            maxLines:
                1, // Define el número máximo de líneas que puede tener el texto
          ),
          const SizedBox(height: 5),
          const TextComponent(
            '\$2,000.00',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          const Divider()
        ],
      ),
    );
  }
}
