import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/components.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final routerProvider = Provider.of<RouterProvider>(context);
    final size = MediaQuery.of(context).size;
    List<Widget> ultimasVentas = [];
    List<Widget> buzonMensajes = [];
    double width1 = (size.width * 0.5) - (routerProvider.getWidthSidebar / 2);
    double width2 = (size.width) - routerProvider.getWidthSidebar;

    Widget cardVentas = CardContainerHomeComponent(
      header: const Row(
        children: [
          Padding(
            padding: EdgeInsets.all(6),
            child: TextComponent('Últimas ventas',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
          )
        ],
      ),
      footer: const Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: EdgeInsets.all(6),
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: TextComponent('Ver todas',
                  style: TextStyle(
                      color: Colors.deepPurple,
                      decoration: TextDecoration.underline)),
            ),
          )
        ],
      ),
      child: Column(
        children: ultimasVentas,
      ),
    );

    Widget cardBuzonMensajes = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(width: 1, color: Colors.deepPurple)),
      child: CardContainerHomeComponent(
        header: const Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.all(6),
              child: TextComponent('Buzón de mensajes',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )),
            ),
            Padding(
              padding: EdgeInsets.all(6),
              child: Row(
                children: [
                  TextComponent('Nuevos '),
                  TextComponent('3',
                      style: TextStyle(
                          color: Colors.deepPurple,
                          fontWeight: FontWeight.bold))
                ],
              ),
            ),
          ],
        ),
        footer: const Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.all(6),
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                child: TextComponent('Ver todas',
                    style: TextStyle(
                        color: Colors.deepPurple,
                        decoration: TextDecoration.underline)),
              ),
            )
          ],
        ),
        child: Column(
          children: buzonMensajes,
        ),
      ),
    );

    Widget firstSection = Column(
      children: [
        SizedBox(
          height: 70,
          child: Row(
            children: [
              Buttons.buttonShape(
                  prefixIcon: Icons.add_outlined,
                  verticalPadding: 8,
                  labelText:
                      size.width > 1540 ? 'Agregar nueva venta' : 'Nueva venta',
                  onPressed: () {
                    //Navigator.pushNamed(context, 'venta');
                    routerProvider.navigate(context, 'venta');

                    routerProvider.setPageActive = 'Ventas';
                  },
                  type: 'primary'),
              const SizedBox(
                width: 19,
              ),
              Buttons.addClient(
                size: size,
                onPressed: () {
                  //if (!loginForm.isValidForm()) return;

                  routerProvider.navigate(context, 'home');
                },
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 25,
        ),
        SizedBox(width: 1000, child: cardVentas),
        const SizedBox(
          height: 25,
        ),
        size.width < 1090 ? cardBuzonMensajes : const SizedBox()
      ],
    );

    for (var i = 0; i < 6; i++) {
      ultimasVentas.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const TextComponent('22-02-2024'),
          Container(
            height: 24.0, // Altura del separador
            width: 1.0, // Grosor del separador
            color: Colors.grey[400], // Color del separador
            margin: const EdgeInsets.symmetric(horizontal: 8.0),
          ),
          const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextComponent(
                'Juan Garcia (#268827)',
              ),
              TextComponent('juan_garcia@email.com',
                  style: TextStyle(fontSize: 12))
            ],
          ),
          const SizedBox(
            width: 20,
          ),
          const TextComponent('\$6,500.00'),
        ],
      ));

      ultimasVentas.add(const Divider());

      // ignore: prefer_const_constructors
      buzonMensajes.add(Container(
        decoration: i < 3
            ? const BoxDecoration(
                border: Border(
                    left: BorderSide(width: 3, color: Colors.deepPurple)))
            : null,
        child: const Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextComponent(
                  'Asunto mensaje buzon 1',
                ),
                TextComponent('20 feb', style: TextStyle(fontSize: 12))
              ],
            ),
            SizedBox(
              width: 20,
            ),
            Icon(Icons.more_vert_outlined)
          ],
        ),
      ));

      buzonMensajes.add(const Divider());
    }

    return ScaffoldComponent(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                width: routerProvider.getIsExpanded
                    ? ((size.width >= 1090)
                        ? ((width1 < 455) ? 455 : width1)
                        : ((width2 < 455) ? 455 : width2))
                    : (size.width >= 1090)
                        ? ((width1 < 455) ? 455 : width1)
                        : ((width2 < 455) ? 455 : width2),
                //size.width > 455 ? size.width * (size.width > 980 ? 0.5 : .92) - (widthSidebar / 2) : 455,
                child: Padding(
                  padding: const EdgeInsets.all(19.0),
                  child: SingleChildScrollView(
                    child: firstSection,
                  ),
                ),
              ),
              size.width >= (routerProvider.getIsExpanded ? 1090 : 900)
                  ? AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      width: width1 < 420 ? 400 : width1,
                      child: Padding(
                        padding: const EdgeInsets.all(19.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            SizedBox(
                              width: size.width * 0.5 -
                                  (routerProvider.getWidthSidebar / 2) -
                                  20,
                              child: Padding(
                                padding: const EdgeInsets.all(19.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const SizedBox(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          TextComponent(
                                              'Miércoles 28 de Febrero',
                                              style: TextStyle(fontSize: 12)),
                                          TextComponent('Bienvenido(a) Irvin',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20)),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 30,
                                    ),
                                    cardBuzonMensajes
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
