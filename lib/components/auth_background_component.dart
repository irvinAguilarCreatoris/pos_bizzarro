import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AuthBackgroundComponent extends StatelessWidget {
  final Widget child;

  const AuthBackgroundComponent({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SizedBox(
      width: double.infinity,
      child: Stack(
        children: [
          size.width > 980 ? const _BoxImage() : const SizedBox(),
          SingleChildScrollView(child: child)
        ],
      ),
    );
  }
}

class _BoxImage extends StatelessWidget {
  const _BoxImage();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width * 0.5,
      height: double.infinity,
      decoration: _boxDecorationTop(),
      child: Center(
        child: Container(
          constraints: const BoxConstraints(
            minWidth: 200,
          ),
          child: AspectRatio(
              aspectRatio: .7,
              child: FractionallySizedBox(
                widthFactor: 1.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    size.height > 250
                        ? SizedBox(
                            child: Column(
                              children: [
                                Image.asset('assets/Logo_Blanco.png'),
                                const SizedBox(
                                  height: 50,
                                ),
                                size.height > 260
                                    ? Center(
                                        child:
                                            Image.asset('assets/Diamante.png'))
                                    : const SizedBox(),
                                const SizedBox()
                              ],
                            ),
                          )
                        : Image.asset('assets/Logo_Blanco.png')
                  ],
                ),
              )),
        ),
      ),
    );
  }

  BoxDecoration _boxDecorationTop() => const BoxDecoration(
          gradient: LinearGradient(colors: [
        Color.fromRGBO(0, 0, 0, 0.90),
        Color.fromRGBO(0, 0, 0, 0.90),
      ]));
}
