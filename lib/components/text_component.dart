import 'package:flutter/material.dart';

class TextComponent extends StatelessWidget {
  final String data;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final int? maxLines;

  const TextComponent(
    this.data, {
    super.key,
    this.style,
    this.textAlign,
    this.overflow,
    this.maxLines,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,
      style: style != null
          ? Theme.of(context).textTheme.bodyLarge?.copyWith(
                fontWeight: style?.fontWeight,
                background: style?.background,
                backgroundColor: style?.backgroundColor,
                color: style?.color,
                debugLabel: style?.debugLabel,
                decoration: style?.decoration,
                decorationColor: style?.decorationColor,
                decorationStyle: style?.decorationStyle,
                decorationThickness: style?.decorationThickness,
                fontFamily: style?.fontFamily,
                fontFamilyFallback: style?.fontFamilyFallback,  
                fontFeatures: style?.fontFeatures,
                fontSize: style?.fontSize,
                fontStyle: style?.fontStyle,
                fontVariations: style?.fontVariations,
                foreground: style?.foreground,
                height: style?.height,
                inherit: style?.inherit,
                leadingDistribution: style?.leadingDistribution,
                letterSpacing: style?.letterSpacing,
                locale: style?.locale,
                overflow: style?.overflow,
                shadows: style?.shadows,
                textBaseline: style?.textBaseline,
                wordSpacing: style?.wordSpacing,
              )
          : Theme.of(context).textTheme.bodyLarge,
    );
  }
}
