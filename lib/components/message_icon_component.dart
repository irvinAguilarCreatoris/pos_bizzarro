import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';

class MessageIcon extends StatelessWidget {
  final int messageCount;

  const MessageIcon({super.key, required this.messageCount});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topRight,
      children: [
        Container(
          padding: const EdgeInsets.only(
            top: 1,
            left: 3,
            right: 3
          ),
          child: const Center(
            child: Icon(
              Icons.mail_outline,
              size: 30.0,
            ),
          ),
        ),
        if (messageCount > 0)
          Container(
            padding: const EdgeInsets.all(3.0),
            decoration: BoxDecoration(
              color: Colors.deepPurple,
              border: Border.all(width: 1, color: Colors.white),
              shape: BoxShape.circle,
            ),
            child: TextComponent(
              messageCount.toString(),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 13.0,
              ),
            ),
          ),
      ],
    );
  }
}
