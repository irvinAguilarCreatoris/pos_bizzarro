import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/components.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/forms/buttons.dart';
import 'package:pos_bizzarro/providers/articulos_provider.dart';
import 'package:pos_bizzarro/utils/utils.dart';
import 'package:provider/provider.dart';

class CardPagoComponent extends StatefulWidget {
  final String clienteNombre;
  final Function? finalizarCompraEvent;
  const CardPagoComponent({
    super.key,
    required this.clienteNombre,
    this.finalizarCompraEvent,
  });

  @override
  State<CardPagoComponent> createState() => _CardPagoComponentState();
}

class _CardPagoComponentState extends State<CardPagoComponent> {
  @override
  Widget build(BuildContext context) {
    final articulosProvider = Provider.of<ArticulosProvider>(context);

    double subtotal = articulosProvider.getTotalPrice;
    double impuestos = articulosProvider.getTaxes;
    double descuentos = articulosProvider.getDiscount;

    double total = subtotal + impuestos - descuentos;

    return SizedBox(
      width: 800,
      child: CardContainerComponent(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextComponent(
              widget.clienteNombre,
              style: const TextStyle(fontSize: 24),
            ),
            const TextComponent(
              'Membresía #5548622895',
              style: TextStyle(fontSize: 14),
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Utils.adjustableTextComponent(context: context, texts: ['Subtotal']),
                Utils.adjustableTextComponent(context: context, 
                    texts: ['\$${Utils.currencyFormatToString(subtotal)}'],
                    firtsTextBold: true, width: 100),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Utils.adjustableTextComponent(context: context, texts: ['Impuestos']),
                Utils.adjustableTextComponent(context: context, 
                    texts: ['\$${Utils.currencyFormatToString(impuestos)}'],
                    firtsTextBold: true, width: 100),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Utils.adjustableTextComponent(context: context, texts: ['Descuentos']),
                Utils.adjustableTextComponent(context: context, 
                    texts: ['\$${Utils.currencyFormatToString(descuentos)}'],
                    firtsTextBold: true, width: 100),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Utils.adjustableTextComponent(context: context, texts: ['Total']),
                Utils.adjustableTextComponent(context: context, 
                    texts: ['\$${Utils.currencyFormatToString(total)}'],
                    firtsTextBold: true, width: 100),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            widget.finalizarCompraEvent != null
                ? Buttons.buttonShape(
                    labelText: 'Pagar ahora',
                    onPressed: () {
                      if (widget.finalizarCompraEvent != null) {
                        widget.finalizarCompraEvent!();
                      }
                    },
                    type: 'primary')
                : const SizedBox()
          ],
        ),
      ),
    );
  }
}
