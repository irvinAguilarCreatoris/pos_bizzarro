import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/message_icon_component.dart';
import 'package:pos_bizzarro/components/text_component.dart';

class Navbar extends StatelessWidget {
  const Navbar({super.key});

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5
          )
        )
      ),
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(
                width: size.width > 430 ? 30 : size.width > 390 ? 15 : 0,
              ),
              size.width > 595 ? const Icon(Icons.logout_outlined) : const SizedBox(),
              size.width > 750 ? const TextComponent('Salir') : const SizedBox(),
              SizedBox(
                width: size.width > 595 ? 30: 0,
              ),
              size.width > 750 ? const TextComponent('Ayuda') : const SizedBox(),
              size.width > 595 ? const Icon(Icons.help_outline_outlined): const SizedBox(),
              SizedBox(
                width: size.width > 430 ? 30 : size.width > 390 ? 15 : 0,
              ),
            ],
          ),
          Row(
            children: [
              size.width > 430
                  ? const TextComponent('Sucursal: centro  |   Caja:03')
                  : size.width > 195 ? const TextComponent('centro  |   03') : const SizedBox(),
              SizedBox(
                width: size.width > 390 ? 30 : 0,
              ),
            ],
          ),
          Row(
            children: [
              size.width > 390 ? const MessageIcon(messageCount: 3) : const SizedBox(),
              SizedBox(
                width: size.width > 390 ? 30 : 0,
              ),
              size.width > 390 ? const CircleAvatar(
                maxRadius: 15,
                // backgroundImage: NetworkImage('https://i.blogs.es/85aa44/stan-lee/840_560.jpg'),
                child: TextComponent('IA'),
              ): const SizedBox(),
              SizedBox(
                width: size.width > 390 ? 15 : 0,
              ),
              size.width > 675 ? const TextComponent("Irvin Aguilar") : const SizedBox(),
              SizedBox(
                width: size.width > 390 ? 15 : 0,
              ),
              const Icon(Icons.settings),
              SizedBox(
                width: size.width > 390 ? 30 : 15,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
