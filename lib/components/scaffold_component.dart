import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/components.dart';

class ScaffoldComponent extends StatelessWidget {
  final Widget child;
  const ScaffoldComponent({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        drawer: size.width > 630 ? null : const Sidebar(),
        body: Row(
          children: [
            const Sidebar(),
            Expanded(
              child: Container(
                  color: const Color.fromARGB(255, 240, 237, 237),
                  width: double.infinity,
                  child: Column(
                    children: [const Navbar(), Expanded(child: child)],
                  )),
            ),
          ],
        ));
  }
}
