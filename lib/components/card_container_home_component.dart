import 'package:flutter/material.dart';

class CardContainerHomeComponent extends StatelessWidget {
  final Widget child;
  final Widget header;
  final Widget footer;

  const CardContainerHomeComponent({
    super.key,
    required this.child,
    required this.header,
    required this.footer,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: _createCardShape(),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(10.0),
            decoration: _createCardShapeHeader(),
            child: header,
          ),
          child, // Este es el contenido principal (body)
          Container(
            padding: const EdgeInsets.all(10.0),
            decoration: _createCardShapeFooter(),
            child: footer,
          ),
        ],
      ),
    );
  }

  BoxDecoration _createCardShape() => BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.white, // Color del body (fondo blanco)
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3),
          ),
        ],
      );

  BoxDecoration _createCardShapeHeader() => BoxDecoration(
        color: Colors.grey[200], // Color del footer (gris)
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(5),
          topRight: Radius.circular(5),
        ),
      );

  BoxDecoration _createCardShapeFooter() => BoxDecoration(
        color: Colors.grey[200], // Color del footer (gris)
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(5),
          bottomRight: Radius.circular(5),
        ),
      );
}
