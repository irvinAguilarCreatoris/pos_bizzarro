import 'package:flutter/material.dart';
import 'package:pos_bizzarro/components/text_component.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:provider/provider.dart';

class Sidebar extends StatefulWidget {
  const Sidebar({super.key});

  @override
  State<Sidebar> createState() => _SidebarState();
}

class _SidebarState extends State<Sidebar> {
  late String selectedOption; // Variable para rastrear la opción seleccionada
  late bool isExpanded; // Variable para rastrear si el sidebar está expandido

  Widget _buildListTile({
    required String title,
    required IconData icon,
    required VoidCallback onTap,
    required bool isSelected,
    required RouterProvider routerProvider,
  }) {
    return ListTile(
      title: isExpanded
          ? TextComponent(
              title,
              style: TextStyle(
                color: isSelected ? Colors.white : Colors.black,
                fontSize: 14,
              ),
            )
          : Icon(
              icon,
              color: isSelected ? Colors.white : Colors.black,
            ),
      leading: isExpanded
          ? Icon(
              icon,
              color: isSelected ? Colors.white : Colors.black,
            )
          : null,
      onTap: () {
        setState(() {
          routerProvider.setPageActive = title;
        });
        onTap();
      },
      tileColor: isSelected ? Colors.blue : null,
      selected: isSelected,
      selectedTileColor: Colors.deepPurple,
      hoverColor: Colors.blue.withOpacity(0.3),
    );
  }

  void expandedEvent(RouterProvider routerProvider) {
    setState(() {
      routerProvider.setIsExpanded = !isExpanded; // Cambia el estado de expansión
      routerProvider.setWidthSidebar = isExpanded ? 70 : 260;
    });
  }

  @override
  Widget build(BuildContext context) {
    final RouterProvider routerProvider = Provider.of<RouterProvider>(context);

    selectedOption = routerProvider.getPageActive;
    isExpanded = routerProvider.getIsExpanded;

    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      width: isExpanded ? 260 : 70,
      child: Drawer(
        backgroundColor: Colors.white,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: Container(
                color: Colors.white,
                child: SizedBox(
                  width: 50.0,
                  child: Image.asset(
                      'assets/${isExpanded ? 'Logo' : 'Logo_B'}.png'),
                ),
              ),
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Inicio',
              icon: Icons.home_outlined,
              onTap: () {
                routerProvider.navigate(context, 'home');

              },
              isSelected: selectedOption == 'Inicio',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Ventas',
              icon: Icons.shopping_bag_outlined,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption == 'Ventas',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Devoluciones',
              icon: Icons.sync_alt_sharp,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption == 'Devoluciones',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Cierre de operaciones',
              icon: Icons.file_copy_outlined,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption == 'Cierre de operaciones',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: isExpanded
                  ? 'Consulta de saldos y pagos de tarjeta de crédito'
                  : '',
              icon: Icons.wallet_outlined,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption ==
                  'Consulta de saldos y pagos de tarjeta de crédito',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Consultar puntos bancarios',
              icon: Icons.account_balance,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption == 'Consultar puntos bancarios',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Generación de cupones',
              icon: Icons.discount_outlined,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption == 'Generación de cupones',
            ),
            _buildListTile(
              routerProvider: routerProvider,
              title: 'Actualización de reparaciones',
              icon: Icons.build_outlined,
              onTap: () {
                // Handle option
              },
              isSelected: selectedOption == 'Actualización de reparaciones',
            ),
            const SizedBox(
              height: 50,
            ),
            ListTile(
              title: isExpanded
                  ? const TextComponent('')
                  : SizedBox(
                      width: 50,
                      child: IconButton(
                        icon: const Icon(Icons.arrow_forward),
                        onPressed: () => expandedEvent(routerProvider)
                      ),
                    ),
              trailing: isExpanded
                  ? SizedBox(
                      width: 50,
                      child: IconButton(
                        icon: const Icon(Icons.arrow_back),
                        onPressed: () => expandedEvent(routerProvider),
                      ),
                    )
                  : const SizedBox(),
              onTap: () => expandedEvent(routerProvider),
            ),
          ],
        ),
      ),
    );
  }
}
