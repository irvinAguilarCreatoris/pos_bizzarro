import 'package:flutter/material.dart' show IconData, Widget;

class MenuOptionModel {

  final String route;
  final IconData icon;
  final String name;
  final Widget page;

  MenuOptionModel({
    required this.route,
    required this.icon,
    required this.name,
    required this.page
  });

}


