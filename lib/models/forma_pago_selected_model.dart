class FormaPagoSelectedModel {

  final int id;
  final String formaPago;
  final String referencia;
  final double importe;
  final bool interfaz;

  FormaPagoSelectedModel({
    required this.id,
    required this.formaPago,
    required this.referencia,
    required this.importe,
    this.interfaz = false
  });

}


