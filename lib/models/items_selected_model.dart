class ItemSelectedModel {

  final int id;
  final String title;
  final String model;
  final double price;
  final int count;

  ItemSelectedModel({
    required this.id,
    required this.title,
    required this.model,
    required this.price,
    required this.count
  });

}


