import 'package:flutter/material.dart';

class ItemExpansionPanelModel {
  ItemExpansionPanelModel({
    required this.expandedValue,
    required this.headerValue,
    this.isExpanded = false,
  });

  Widget expandedValue;
  String headerValue;
  bool isExpanded;
}
