import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pos_bizzarro/providers/articulos_provider.dart';
import 'package:pos_bizzarro/providers/pagos_provider.dart';
import 'package:pos_bizzarro/providers/router_provider.dart';
import 'package:pos_bizzarro/router/app_routes.dart';
import 'package:pos_bizzarro/theme/app_theme.dart';
import 'package:provider/provider.dart';

void main() => runApp(const AppState());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      scrollBehavior: const MaterialScrollBehavior().copyWith(
        dragDevices: {PointerDeviceKind.mouse, PointerDeviceKind.touch},
      ),
      title: 'Bizzarro',
      initialRoute: AppRoutes.initialRoute,
      routes: AppRoutes.getAppRoutes(),
      onGenerateRoute: AppRoutes.onGenerateRoute,
      theme: AppTheme.lightTheme,
    );
  }
}

class AppState extends StatelessWidget {
  const AppState({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => RouterProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ArticulosProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PagosProvider(),
        )
      ],
      child: const MyApp(),
    );
  }
}
